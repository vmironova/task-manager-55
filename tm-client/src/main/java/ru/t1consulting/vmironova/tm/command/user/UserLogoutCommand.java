package ru.t1consulting.vmironova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.dto.request.UserLogoutRequest;
import ru.t1consulting.vmironova.tm.enumerated.Role;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Logout current user.";

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        authEndpoint.logoutUser(request);
        setToken(null);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
