package ru.t1consulting.vmironova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.enumerated.Status;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + project.getCreated());
    }

}
