package ru.t1consulting.vmironova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.dto.request.UserLockRequest;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

@Component
public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "User lock.";

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        userEndpoint.lockUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
