package ru.t1consulting.vmironova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1consulting.vmironova.tm.api.service.IConnectionService;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.api.service.dto.IProjectDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.ITaskDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.IUserDTOService;
import ru.t1consulting.vmironova.tm.comparator.NameComparator;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;
import ru.t1consulting.vmironova.tm.repository.dto.TaskDTORepository;
import ru.t1consulting.vmironova.tm.service.ConnectionService;
import ru.t1consulting.vmironova.tm.service.PropertyService;
import ru.t1consulting.vmironova.tm.service.dto.ProjectDTOService;
import ru.t1consulting.vmironova.tm.service.dto.TaskDTOService;
import ru.t1consulting.vmironova.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1consulting.vmironova.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1consulting.vmironova.tm.constant.TaskTestData.*;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class TaskDTORepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private static ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return CONNECTION_SERVICE.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        PROJECT_SERVICE.remove(USER_ID, USER_PROJECT1);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Before
    public void before() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER_ID, USER_TASK1);
            repository.add(USER_ID, USER_TASK2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER_ID);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(USER_ID, USER_TASK3));
            entityManager.getTransaction().commit();
            @Nullable final TaskDTO task = repository.findOneById(USER_ID, USER_TASK3.getId());
            Assert.assertNotNull(task);
            Assert.assertEquals(USER_TASK3.getId(), task.getId());
            Assert.assertEquals(USER_ID, task.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final TaskDTO task = repository.create(USER_ID, USER_TASK3.getName());
            Assert.assertEquals(USER_TASK3.getName(), task.getName());
            Assert.assertEquals(USER_ID, task.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final TaskDTO task = repository.create(USER_ID, USER_TASK3.getName(), USER_TASK3.getDescription());
            Assert.assertEquals(USER_TASK3.getName(), task.getName());
            Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
            Assert.assertEquals(USER_ID, task.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<TaskDTO> tasks = repository.findAll(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
        entityManager.close();
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<TaskDTO> tasks = repository.findAll(USER_ID, comparator);
        Assert.assertNotNull(tasks);
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
        entityManager.close();
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(USER_ID, NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_ID, USER_TASK1.getId()));
        entityManager.close();
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(USER_ID, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = repository.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
        entityManager.close();
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER_ID);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(USER_ID));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(USER_ID, USER_TASK2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_TASK2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(USER_ID));
        entityManager.close();
    }

    @Test
    public void update() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            USER_TASK1.setName(USER_TASK3.getName());
            repository.update(USER_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_TASK3.getName(), repository.findOneById(USER_ID, USER_TASK1.getId()).getName());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
